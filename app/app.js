import Vue from "nativescript-vue";

import Home from "./pages/Home";
import LoginScreen from "./pages/Loginscreen"

Vue.registerElement("Gradient", () => require("nativescript-gradient").Gradient);

new Vue({

    template: `
        <Frame>
            <Home />
        </Frame>`,

    components: {
        Home
    }
}).$start();
