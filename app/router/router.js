import Vue from 'nativescript-vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import LoginScreen from '../pages/Loginscreen'

const router = new VueRouter({
    pageRouting: true,
    routes: [
        {
            path: '/home',
            component: Home,
            meta: {
                title: 'home',
            },
        },
        {
            path: '/login',
            component: LoginScreen,
            meta: {
                title: 'loginscreen',
            },
        },
        {path: '*', redirect: '/home'},
    ],
});

router.replace('/home');

module.exports = router;
